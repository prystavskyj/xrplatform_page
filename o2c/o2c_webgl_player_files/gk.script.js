/*

#------------------------------------------------------------------------

# Corporate - April 2010 (for Joomla 1.5)

#

# Copyright (C) 2007-2010 Gavick.com. All Rights Reserved.

# License: Copyrighted Commercial Software

# Website: http://www.gavick.com

# Support: support@gavick.com   

#------------------------------------------------------------------------ 

# Based on T3 Framework

#------------------------------------------------------------------------

# Copyright (C) 2004-2009 J.O.O.M Solutions Co., Ltd. All Rights Reserved.

# @license - GNU/GPL, http://www.gnu.org/copyleft/gpl.html

# Author: J.O.O.M Solutions Co., Ltd

# Websites: http://www.joomlart.com - http://www.joomlancers.com

#------------------------------------------------------------------------

*/



//Call noconflict if detect jquery

//Apply jquery.noConflict if jquery is enabled

if ($defined(window.jQuery) && $type(jQuery.noConflict)=='function') {

	jQuery.noConflict();

}



function switchFontSize (ckname,val){

	var bd = $E('body');

	switch (val) {

		case 'inc':

			if (CurrentFontSize+1 < 7) {

				bd.removeClass('fs'+CurrentFontSize);

				CurrentFontSize++;

				bd.addClass('fs'+CurrentFontSize);

			}		

		break;

		case 'dec':

			if (CurrentFontSize-1 > 0) {

				bd.removeClass('fs'+CurrentFontSize);

				CurrentFontSize--;

				bd.addClass('fs'+CurrentFontSize);

			}		

		break;

		default:

			bd.removeClass('fs'+CurrentFontSize);

			CurrentFontSize = val;

			bd.addClass('fs'+CurrentFontSize);		

	}

	Cookie.set(ckname, CurrentFontSize,{duration:365});

}



function switchTool (ckname, val) {

	createCookie(ckname, val, 365);

	window.location.reload();

}



function createCookie(name,value,days) {

  if (days) {

    var date = new Date();

    date.setTime(date.getTime()+(days*24*60*60*1000));

    var expires = "; expires="+date.toGMTString();

  }else expires = "";

  document.cookie = name+"="+value+expires+"; path=/";

}

//addEvent - attach a function to an event

function gkAddEvent(obj, evType, fn){ 

 if (obj.addEventListener){ 

   obj.addEventListener(evType, fn, false); 

   return true; 

 } else if (obj.attachEvent){ 

   var r = obj.attachEvent("on"+evType, fn); 

   return r; 

 } else { 

   return false; 

 } 

}



window.addEvent('load', function() {

	new SmoothScroll(); 

	var login = false;

	var register = false;

	var tools = false;

	var login_fx = null;

	var hlogin_fx = null;

	var register_fx = null;

	var hregister_fx = null;

	var tools_fx = null;

	var link_login_fx = null;

	var link_reg_fx = null;

	var login_over = false;

	var register_over = false;



	if($('btn_login')){
		$('popup_login').setStyle('display', 'block');
		var loginh = $('popup_login').getSize().size.y;

		login_fx = new Fx.Style($('popup_login'),'opacity',{duration:300}).set(0);

		hlogin_fx = new Fx.Style($('popup_login'),'height',{duration:300}).set(0);

		

		$('popup_login').setStyle('display','block');

		$('btn_login').addEvent('click', function(e){

			new Event(e).stop();

			if(!login){

				login_fx.start(1);

				hlogin_fx.start(loginh);

				login = true;	

				$('btn_login').addClass('popup');

			}else{

				login_fx.start(0);

				hlogin_fx.start(0);

				login = false;

				$('btn_login').removeClass('popup');

			}

			

			if(register){

				register_fx.start(0);

				hregister_fx.start(0);

				register = false;

				$('btn_register').removeClass("popup");

			}



			$('popup_login').setStyles({

				"left" : ($('btn_login').getCoordinates().left + $('btn_login').getCoordinates().width - $('popup_login').getCoordinates().width) + "px",

				"top" : $('btn_login').getCoordinates().top + "px"

			});

			

			$('popup_login').getElement('.gk_popup_wrap').addEvent('mouseover',function(){login_over = true;});

            $('popup_login').getElement('.gk_popup_wrap').addEvent('mouseout',function(){login_over = false;});

		});

	}



	if($('btn_register')){
		$('popup_register').setStyle('display', 'block');
		var registerh = $('popup_register').getSize().size.y;

    	register_fx = new Fx.Style($('popup_register'),'opacity',{duration:300}).set(0);

		hregister_fx = new Fx.Style($('popup_register'),'height',{duration:300}).set(0);

		$('popup_register').setStyle('display','block');

		$('btn_register').addEvent('click', function(e){

			new Event(e).stop();

			if(!register){

				register_fx.start(1);

				hregister_fx.start(registerh);

				register = true;	

				$('btn_register').addClass("popup");		

			}else{

				register_fx.start(0);

				hregister_fx.start(0);

				register = false;

				$('btn_register').removeClass("popup");

			}

			

			if(login){

				login_fx.start(0);

				hlogin_fx.start(0);

				login = false;

    		  	$('btn_login').removeClass("popup");

			}



			$('popup_register').setStyles({

				"left" : ($('btn_register').getCoordinates().left + $('btn_register').getCoordinates().width - $('popup_register').getCoordinates().width) + "px",

				"top" : $('btn_register').getCoordinates().top + "px"

			});

			

			$('popup_register').getElement('.gk_popup_wrap').addEvent('mouseover',function(){register_over = true;});

            $('popup_register').getElement('.gk_popup_wrap').addEvent('mouseout',function(){register_over = false;});

		});	

	}



	if($('btn_tools')){

		var opened = false;

		if($('btn_login')) link_login_fx = new Fx.Style($('btn_login'),'opacity',{duration:300});

		if($('btn_register')) link_reg_fx = new Fx.Style($('btn_register'),'opacity',{duration:300});

		

		$('popup_tools').getParent().setProperty('class','gk_hide').setStyles({

			'display' : 'block'

		});



		$('popup_tools').setStyle('display', 'block');

		tools_fx = new Fx.Style($('popup_tools').getParent(),'width',{duration:300}).set(0);

		$('btn_tools').addEvent('click', function(e){

			new Event(e).stop();

			tools_fx.start((opened) ? 0 : 135);

			if($('btn_login')) link_login_fx.start(opened ? 1 : 0);

			if($('btn_register')) link_reg_fx.start(opened ? 1 : 0);

			opened = !opened;



			if(login){

				login_fx.start(0);

				hlogin_fx.start(0);

				login = false;

				$('btn_login').removeClass("popup");

			}



			if(register){

				register_fx.start(0);

				hregister_fx.start(0);

				register = false;

				$('btn_register').removeClass("popup");

			}

		});	

	}

	//

	if($('stylearea')){

		$A($$('.style_switcher')).each(function(element,index){

			element.addEvent('click',function(event){

				var event = new Event(event);

				event.preventDefault();

				changeStyle(index+1);

			});

		});

		new SmoothScroll();

	}

	

	$(document.body).addEvent("click", function(e){

		if(login && !login_over){

			login_fx.start(0);

			hlogin_fx.start(0);

			login = false;

			$('btn_login').removeClass("popup");

		}

		if(register && !register_over){

			register_fx.start(0);

			hregister_fx.start(0);

			register = false;

			$('btn_register').removeClass("popup");

		}

	});

});



// Function to change styles

function changeStyle(style){

	var file = tmplurl+'/css/style'+style+'.css';

	new Asset.css(file);

	new Cookie.set('gk36_style',style,{duration: 200,path: "/"});

	(function(){if(Cufon) Cufon.refresh();}).delay(500);

}

// JCaptionCheck

function JCaptionCheck(){ return (typeof(JCaption) == "undefined")?  false: true; }



if(!JCaptionCheck()) {

	var JCaption = new Class({

		initialize: function(selector) {

			this.selector = selector;

			var images = $$(selector);

			images.each(function(image){ this.createCaption(image); }, this);

		},



		createCaption: function(element) {

			var caption   = document.createTextNode(element.title);

			var container = document.createElement("div");

			var text      = document.createElement("p");

			var width     = element.getAttribute("width");

			var align     = element.getAttribute("align");

			var docMode = document.documentMode;

			//Windows fix

			if (!align)

				align = element.getStyle("float");  // Rest of the world fix

			if (!align) // IE DOM Fix

				align = element.style.styleFloat;

			text.appendChild(caption);

			text.className = this.selector.replace('.', '_');

			

			if (align=="none") {

				if (element.title != "") {

					element.parentNode.replaceChild(text, element);

					text.parentNode.insertBefore(element, text);

				}

			} else {

				element.parentNode.insertBefore(container, element);

				container.appendChild(element);

				if ( element.title != "" ) { container.appendChild(text); }

				container.className   = this.selector.replace('.', '_');

				container.className   = container.className + " " + align;

				container.setAttribute("style","float:"+align);

				//IE8 fix

				if (!docMode|| docMode < 8) {

					container.style.width = width + "px";

				}

			}

		}

	});



	document.caption = null;

	window.addEvent('load', function() {

		var caption = new JCaption('img.caption')

		document.caption = caption

	});

}