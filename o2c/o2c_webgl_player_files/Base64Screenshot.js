function DumpFramebuffer(width, height, engine) {
  // Read the contents of the framebuffer
  var numberOfChannelsByLine = width * 4;
  var halfHeight = height / 2;
  //Reading datas from WebGL
  var data = engine.readPixels(0, 0, width, height);
  //To flip image on Y axis.
  for (var i = 0; i < halfHeight; i++) {
    for (var j = 0; j < numberOfChannelsByLine; j++) {
      var currentCell = j + i * numberOfChannelsByLine;
      var targetLine = height - i - 1;
      var targetCell = j + targetLine * numberOfChannelsByLine;
      var temp = data[currentCell];
      data[currentCell] = data[targetCell];
      data[targetCell] = temp;
    }
  }
  // Create a 2D canvas to store the result
  if (typeof screenshotCanvas == "undefined") {
    screenshotCanvas = document.createElement('canvas');
  }
  screenshotCanvas.width = width;
  screenshotCanvas.height = height;
  var context = screenshotCanvas.getContext('2d');
  // Copy the pixels to a 2D canvas
  var imageData = context.createImageData(width, height);
  //cast is due to ts error in lib.d.ts, see here - https://github.com/Microsoft/TypeScript/issues/949
  var castData = imageData.data;
  castData.set(data);
  context.putImageData(imageData, 0, 0);
  return screenshotCanvas.toDataURL();
}

function RenderBabylonSceneToBase64(engine, camera, size,Callback) {
  var width;
  var height;
  var scene = camera.getScene();
  var previousCamera = null;
  if (scene.activeCamera !== camera) {
    previousCamera = scene.activeCamera;
    scene.activeCamera = camera;
  }
  //If a precision value is specified
  if (size.precision) {
    width = Math.round(engine.getRenderWidth() * size.precision);
    height = Math.round(width / engine.getAspectRatio(camera));
    size = { width: width, height: height };
  }
  else if (size.width && size.height) {
    width = size.width;
    height = size.height;
  }
  else if (size.width && !size.height) {
    width = size.width;
    height = Math.round(width / engine.getAspectRatio(camera));
    size = { width: width, height: height };
  }
  else if (size.height && !size.width) {
    height = size.height;
    width = Math.round(height * engine.getAspectRatio(camera));
    size = { width: width, height: height };
  }
  else if (!isNaN(size)) {
    height = size;
    width = size;
  }
  else {
    Tools.Error("Invalid 'size' parameter !");
    return;
  }
  
  //At this point size can be a number, or an object (according to engine.prototype.createRenderTargetTexture method)
  var texture = new BABYLON.RenderTargetTexture("screenShot", size, scene, false, false);
  texture.renderList = scene.meshes;
  texture.onAfterRender = function () {
    Callback(DumpFramebuffer(width, height, engine));
    engine.resize();
    scene.render();
  };
  scene.incrementRenderId();
  texture.render(true);
  texture.dispose();
  if (previousCamera) {
    scene.activeCamera = previousCamera;
  }
};