

function ShowTooltip() {
  if (typeof scene == "undefined" || !scene) return;
  var pickInfo = scene.pick(scene.pointerX, scene.pointerY); // Get pickinfo from the current mouseposition
  var canvas   = scene.getEngine()._renderingCanvas; // Get current Canvas (for tooltip)
  var pMesh,pSubMeshID,pSubMesh,pMaterial;
  
  if (pickInfo.hit && pickInfo.pickedMesh.material.subMaterials) {  // any mesh hit?
    pMesh      = pickInfo.pickedMesh;                     // Picked mesh
    pSubMeshID = pickInfo.subMeshId;                      // Submesh id
    pSubMesh   = pMesh.subMeshes[pSubMeshID];             // current submesh
    pMaterial  = pMesh.material.subMaterials[pSubMeshID]; // current materal
    var text = pMaterial.name + " FaceId=" + pickInfo.faceId;
    if (pMesh.SubObNames && pMesh.SubObInds && (pMesh.SubObInds.length > pickInfo.faceId))
      text = pMesh.SubObNames[pMesh.SubObInds[pickInfo.faceId]];
    // Show the tooltip
    if(canvas.tooltip)
      canvas.tooltip.show(text,scene.pointerX, scene.pointerY,canvas.wasTouched);
  } else if(canvas.tooltip){
    canvas.tooltip.hide(); // Hide the tooltip
  }
  else{
    //console.log("canvas.tooltip is not defined in this version of babylon.js. Tarasprystavskyj@gmail.com")
  }
}

function onMouseMove() { // Scene has to be defined!
  if (typeof scene == "undefined" || !scene) return;
  var canvas = scene.getEngine()._renderingCanvas; // Get current Canvas (for tooltip)
  canvas.wasTouched = false;
  if(canvas.tooltip)
    canvas.tooltip.hide(); // Hide the tooltip
  window.clearTimeout(canvas.showTooltipID); // Clear old timeout to prevent showing the wrong tooltip
  
  canvas.showTooltipID = window.setTimeout(ShowTooltip,750); // <== This is the time to wait before showing the tooltip (in ms)
}

function onTouchStart() {
  if (typeof scene == "undefined" || !scene) return;
  var canvas = scene.getEngine()._renderingCanvas; // Get current Canvas (for tooltip)
  canvas.wasTouched = true;
  
  canvas.showTooltipID = window.setTimeout(ShowTooltip,750); // <== This is the time to wait before showing the tooltip (in ms)
}

function onTouchMove() {
  if (typeof scene == "undefined" || !scene) return;
  var canvas = scene.getEngine()._renderingCanvas; // Get current Canvas (for tooltip)

  canvas.tooltip.hide(); // Hide the tooltip
  window.clearTimeout(canvas.showTooltipID); // Clear old timeout to prevent showing the wrong tooltip
}


function ActivateMaterialSelect(Actvate) {
  var canvas = scene.getEngine()._renderingCanvas; // Get the current canvas to attach eventlisteners
  
  if (!canvas.tooltip) {
    var T = canvas.tooltip = document.createElement("div");
    var C = document.createElement("div");
    
    C.setAttribute("style","-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; width: 10px; height: 10px;");
    
    T.style.backgroundColor                 = "rgba(255,255,255,0.9)";
    T.style.border                          = "solid 1px grey";
    C.style.position   = T.style.position   = "absolute";
    C.style.display    = T.style.display    = "none";
    T.style.padding                         = "3px";
    C.style.opacity    = T.style.opacity    = "0";
    C.style.transition = T.style.transition = "all 0.3s";
    T.style.fontSize                        = "11px";
    C.shown            = T.shown            = false;
    
    C.style.backgroundColor = "rgb(51, 103, 214)";
    
    T.myClickPoint = C;
    
    T.show = function Show(Text,X,Y,ShowCP) {
      var CP             = canvas.getBoundingClientRect();
      this.style.left    = (CP.left + X + 14) + "px";
      this.style.top     = (CP.top  + Y) +"px";
      this.style.display = "inline";
      setTimeout(function setOpacityTo1() {T.style.opacity = "1";},10);
      this.textContent = Text;
      this.shown = true;
      
      if (ShowCP) {
        this.myClickPoint.style.left    = (CP.left + X - 5) + "px";
        this.myClickPoint.style.top     = (CP.top  + Y - 5) +"px";
        this.myClickPoint.style.display = "inline";
        setTimeout(function setOpacityTo1() {T.myClickPoint.style.opacity = "1";},10);
      }
    };
    
    T.hide = function Hide() {
      if (!this.shown) return;
      T.style.opacity = "0";
      T.myClickPoint.style.opacity = "0";
      setTimeout(function setDisplayNone() {T.style.display = "none";},300);
      setTimeout(function setDisplayNone() {T.myClickPoint.style.display = "none";},300);
      this.shown = false;
      this.myClickPoint.style.display = "inline";
    };
    
    document.body.appendChild(T);
    document.body.appendChild(C);
  }
  
  if (Actvate) {
    canvas.addEventListener("mousemove", onMouseMove, false); // Add the move event
    canvas.addEventListener("touchmove", onTouchMove, false); // Add the move event
    canvas.addEventListener("touchstart", onTouchStart, false); // Add the move event
  } else {
    canvas.removeEventListener("mousemove", onMouseMove, false); // remove the move event
    canvas.removeEventListener("touchmove", onTouchMove, false); // remove the move event
    canvas.removeEventListener("touchstart", onTouchStart, false); // remove the move event
    canvas.tooltip.hide(); // Hide the tooltip
    window.clearTimeout(canvas.showTooltipID); // Clear old timeout to prevent showing the wrong tooltip
  }
  
}